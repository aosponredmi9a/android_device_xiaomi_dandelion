# vendor_overlay
# See https://source.android.google.cn/devices/bootloader/partitions/system-as-root#using-vendor-overlay
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(DEVICE_PATH)/vendor_overlay,$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay)

